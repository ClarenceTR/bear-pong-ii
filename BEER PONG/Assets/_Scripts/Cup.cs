﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cup : MonoBehaviour
{
    [SerializeField]
    float drunkenessMin = 8;

    [SerializeField]
    float drunkenessMax = 12;

    public float CupDrunkenness
    {
        get
        {
            if (drunkenessMin <= drunkenessMax)
                return Random.Range(drunkenessMin, drunkenessMax);
            else
                return drunkenessMax;
        }
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
