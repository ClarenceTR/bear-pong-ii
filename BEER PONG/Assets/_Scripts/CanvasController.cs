﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class CanvasController : Singleton<CanvasController>
{
    private bool isPaused;      //Used to track whether the game is "Paused" or not

    [SerializeField]
    private GameObject pauseMessage;

    [SerializeField]
    private GameObject pausePrompt;

    [SerializeField]
    private GameObject controlsPrompt;

    [SerializeField]
    private GameObject continuePrompt;

    [SerializeField]
    private GameObject pauseMaskPanel;

    [SerializeField]
    private GameObject ball;


    //Initializes class fields
    // ...NOTE: the "PMenu" does not need to be initialized as its reference has been set within Unity's Inspector Pane
    private void Awake()
    {
        isPaused = false;

    }//End Awake




    // Update performs a check to see if the player "Paused" the game
    //if so, then the game time is stopped and the Pause Menu is Displayed
    //Update also checks for User Input when the game is Paused to determine
    //if the Main Menu should be loaded
    private void Update()
    {
        if (isPaused)
        {
            PauseGame(true); //Pauses the game
        }
        else
        {
            PauseGame(false); //Un-Pauses the game
        }

        //"if" the player pressed the "Pause Button" ...
        if (Input.GetButtonDown("Cancel"))
        {
            //Print that the Pause Button was pressed to the Console for debuggin purposes
            //print("Pause Button PRESSED !!!");

            //Change the state of the "isPaused" boolean via the "SwitchPause()" function
            SwitchPause();
        }

        //"if" the game is paused AND the player chooses to Quit back to the Start Menu ...
        if (Input.GetButtonDown("LoadMainMenu") && isPaused)
        {
            //Laod the Start_Menu scene
            SceneManager.LoadScene("Scene00");
        }

        if (ball.GetComponent<Rigidbody>().isKinematic)
        {
            controlsPrompt.SetActive(true);
            pausePrompt.SetActive(true);
        }
        else
        {
            controlsPrompt.SetActive(false);
            pausePrompt.SetActive(false);
        }

        if (GameManager.Instance.AbleToProceedToNextRound)
        {
            continuePrompt.SetActive(true);
        }
        else
        {
            continuePrompt.SetActive(false);

        }


    }//End Update()




    //Toggles the Pause State of the Game
    public void SwitchPause()
    {
        //if (isPaused) //"if" isPaused is true
        //{
        //    isPaused = false; //set it to false
        //}
        //else //set it to true
        //{
        //    isPaused = true;
        //}
        isPaused = !isPaused;
    }//End SwitchPause()




    //Pauses the Game or Un-Pauses the game
    private void PauseGame(bool state)
    {
        if (state) //"if" the game should be paused (i.e. "isPaused" = true)
        {
            GameManager.Instance.MouseFreeMove = true;
            pauseMessage.SetActive(true); //set the "PMenu" UI Canvas to "active" so that it displays to the screen
            pauseMaskPanel.SetActive(true); // set the "Mask Panel" UI object to "active" 
            Time.timeScale = 0.0f; //and then pause the game
        }
        else //De-Activate the PMenu UI Canvas and Un-Pause the game
        {
            Time.timeScale = 1.0f;
            pauseMessage.SetActive(false);
            pauseMaskPanel.SetActive(false);
            GameManager.Instance.MouseFreeMove = false;
        }
    }//End PauseGame()


}