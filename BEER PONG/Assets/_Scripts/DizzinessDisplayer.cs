﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DizzinessDisplayer : MonoBehaviour {

    [SerializeField]
    GameObject dizzinessUIPrefab;

    [SerializeField]
    Character player;

    private void Start()
    {
    }

    public void CheckDizziness(float time = .0f)
    {
        if (time > 0)
        {
            Invoke("CheckDizziness", time);return;
        }

        int howMany = 10 - player.Cups.Count;

        foreach (Transform t in transform)
        {
            Destroy(t.gameObject);
        }

        for (int i = 0; i < howMany; i++)
        {
            GameObject dizziness = Instantiate(dizzinessUIPrefab);
            dizziness.transform.parent = transform;
        }
    }
}
