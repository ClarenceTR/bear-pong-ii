﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LandedLine : MonoBehaviour {

    GameManager GM;

    [SerializeField]
    GameObject lastPosPrefab;
    GameObject lastPos;

    private void Start()
    {
        GM = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
    }

    public void SetBallLandedLineRed()
    {
        if (GM.indexOfActivePlayer == 1)
        {
            //LineRenderer lineRenderer = GetComponent<LineRenderer>();
            //lineRenderer.SetVertexCount(2);
            //lineRenderer.material.color = Color.red;
            //lineRenderer.SetPosition(1, GM.RecordBallLanded());

            Destroy(lastPos);
            lastPos = Instantiate(lastPosPrefab, GM.RecordBallLanded(), Quaternion.identity);
        }

    }
}
