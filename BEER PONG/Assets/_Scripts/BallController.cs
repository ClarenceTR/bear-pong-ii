﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    Rigidbody rb;

    [SerializeField]
    [Range(0, 150f)]
    float initialSpeed = 150.0f; // Every round, ball's starting speed.

    [SerializeField]
    float currentSpeed;

    [SerializeField]
    [Range(150, 250f)]
    float maxSpeed;

    [SerializeField]
    [Range(100, 150f)]
    float minSpeed;

    [SerializeField]
    float stopSpeedThreshold = 2f;

    int framesToWairAfterLaunchToCheckSpeed = 20;

    bool ballLaunched;

    AudioSource audioSource;
    [SerializeField]
    private List<AudioClip> audioclips;

    [SerializeField]
    BarController powerBar;

    int bouncedTime;

    /// <summary>
    /// After how many times the player can choose to skip watching the ball bouncing and proceed to the next round.
    /// </summary>
    [SerializeField]
    int bouncedTimeSkipThreshold = 3;   

    public bool BallLaunched
    {
        set
        {
            ballLaunched = value;
            if (ballLaunched)
                StartCoroutine(CheckBallSpeed(framesToWairAfterLaunchToCheckSpeed));
        }
    }

    public float Thrust
    {
        get
        {
            return currentSpeed;
        }
        set
        {
            currentSpeed = Mathf.Clamp(value, 0, MaxPower);
            powerBar.Value = currentSpeed-minSpeed;
        }
    }

    public float MaxPower
    {
        get
        {
            return maxSpeed;
        }
        set
        {
            maxSpeed = value;
            powerBar.MaxValue = maxSpeed-minSpeed;
        }
    }

    public float Speed
    {
        get
        {
            return GetComponent<Rigidbody>().velocity.magnitude;
        }
    }

    IEnumerator CheckBallSpeed(float waitingFrames)
    {
        for (int i = 0; i < waitingFrames; i++)
        {
            yield return new WaitForFixedUpdate();
        }
        while (Speed > stopSpeedThreshold)
        {
            yield return new WaitForFixedUpdate();
        }
        GameManager.Instance.NextRound(4f);
    }

    public Rigidbody Rb
    {
        get
        {
            return rb;
        }

        set
        {
            rb = value;
        }
    }

    private void Awake()
    {
        //InactivateBall();
        audioSource = GetComponent<AudioSource>();
    }

    public void ResetStatus()
    {
        rb = GetComponent<Rigidbody>();
        rb.isKinematic = true;
        rb.velocity = Vector3.zero;

        MaxPower = maxSpeed;
        Thrust = initialSpeed;

        bouncedTime = 0;
    }

    private void Start()
    {
        ResetStatus();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Cup"))
        {
            audioSource.PlayOneShot(audioclips[0]);
            // Drink up the cup of wine
            Character player = other.transform.parent.GetComponent<Character>();
            float howMuchDrunkennessGot = other.GetComponent<Cup>().CupDrunkenness;
            player.CurrentDrunkenness += howMuchDrunkennessGot;

            // remove the cup
            player.Cups.Remove(other.GetComponent<Cup>());
            Destroy(other.gameObject);

            GameManager.Instance.NextRound(2f);

            // Update UI
            player.UpdateCupDisplay();
            player.UpdateDizzinessDisplay();
        }

        if (other.CompareTag("Floor"))
        {
            // go to next round
            GameManager.Instance.NextRound(2f);
        }
    }

    /// <summary>
    /// Plays the SFX of the Ball colliding with a Surface
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionEnter(Collision collision)
    {
        audioSource.Play();

        // Set "AbleToProceedToNextRound" to true so that players can skip the observation of the ball and head into the next round.
        if (collision.collider.CompareTag("Floor"))
        {
            GameManager.Instance.AbleToProceedToNextRound = true;
        }
        if (!collision.collider.CompareTag("Cup"))
        {
            bouncedTime += 1;
            if(bouncedTime>=bouncedTimeSkipThreshold)
                GameManager.Instance.AbleToProceedToNextRound = true;
        }
    }






}
