﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.PostProcessing;

public class CameraController : Singleton<CameraController>
{
    // ***********************************************************
    // Member Fields/Variables for the CameraController Object
    // ***********************************************************
    private GameObject ball;

    private Vector3 offset;

    private AudioSource audioSource;

    //private AudioSource audioSource2;

    private AudioSource[] musicList;

    //[SerializeField]
    //private List<AudioClip> audioclipBGSound;

    private int musicListIndex;

    [SerializeField]
    private Vector2 drunknessDirection;
    [SerializeField]
    private float drunknessIncrementX;
    [SerializeField]
    private float drunknessIncrementY;

    //Post Processing related Variables 
    private PostProcessingProfile m_Profile;

    private float currentDrunkness;

    [SerializeField]
    private Bounds drunkenBounds;

    private Character m_player;

    [SerializeField]
    private GameManager m_GameManager;

    // ************************************************************
    // END: Member Fields/Variables
    // ************************************************************




    // ************************************************************
    // Start Property Definitions
    // ************************************************************

    /// <summary>
    /// Property "Ball" - used to hold reference to the Ball in the Scene
    /// </summary>
    public GameObject Ball
    {
        get
        {
            return ball;
        }

        set
        {
            ball = value;
        }
    }


    /// <summary>
    /// Property "Focus" - boolean that calls "FocusWhenDrunk()" fucntion, when "true"
    /// "FocusWhenDrunk()" tempoarily decreases the semi-randomized movement of the "Camera" GameObject when the player is "DRUNK"
    /// </summary>
    public bool Focus
    {
        set
        {
            if (value)
            {
                StartCoroutine(FocusWhenDrunk());
            }
            else
            {
                StopCoroutine(FocusWhenDrunk());
                SetDrunkness(currentDrunkness);
            }
        }
    }


    // ************************************************************
    // END: Property Definitions
    // ************************************************************





    /// <summary>
    /// Initializes Member Fields for the CameraController Object
    /// Prior to the First Frame of the Scene being rendered
    /// Runs prior to all GameObjects' Start() functions
    /// </summary>
    private void Awake()
    {
        //audioSource2 = GetComponent<AudioSource>();
    }



    /// <summary>
    /// Initialization Functions run at the First Frame of the Scene being Rendered
    /// </summary>
    private void Start()
    {
        //audioSource2.clip = audioclipBGSound[0];
        //audioSource2.Play();
        Ball = GameObject.FindGameObjectWithTag("Ball");
        SetCameraOffset();

        musicListIndex = 0;
        musicList = GetComponents<AudioSource>();
        audioSource = musicList[musicListIndex];

        m_Profile = (PostProcessingProfile)Instantiate(Resources.Load("_PostProcessing_Profiles\\Sober_Vision"));

        gameObject.AddComponent<PostProcessingBehaviour>();
        GetComponent<PostProcessingBehaviour>().profile = m_Profile;

    }


    /********************************************************************
         *  START: Section to Manipulate Post Processing Effects on Camera
         *      and Game Music
         * 
         */
    void Update()
    {
        if (!audioSource.isPlaying)
        {
            audioSource = musicList[musicListIndex];
            audioSource.enabled = false;

            musicListIndex = (musicListIndex + 1) % musicList.Length;

            audioSource = musicList[musicListIndex];
            audioSource.enabled = true;
            audioSource.Play();
        }


        /*
         * EXAMPLE:  Runtime Manipulation of PostProcessing Effects on the Camera
        var vignette = m_Profile.vignette.settings;
        vignette.smoothness = Mathf.Abs(Mathf.Sin(Time.realtimeSinceStartup) * 0.99f) + 0.01f;
        m_Profile.vignette.settings = vignette;
        */

        if (m_GameManager.Players.Count > 1)
        {
            m_player = m_GameManager.ActivePlayer;
        }


        if (m_player.Cups.Count > 8)
        {
            m_Profile.motionBlur.enabled = false;
            m_Profile.depthOfField.enabled = false;
            m_Profile.vignette.enabled = false;
            m_Profile.grain.enabled = false;

            if (audioSource.isPlaying)
            {
                audioSource.pitch = 1.0f;
                audioSource.volume = 1.0f;
            }
        }



        if (m_player.Cups.Count <= 8)
        {
            var motionBlur = m_Profile.motionBlur.settings;
            motionBlur.shutterAngle = 360;
            motionBlur.sampleCount = 28;
            motionBlur.frameBlending = 0.50f;
            m_Profile.motionBlur.settings = motionBlur;
            m_Profile.motionBlur.enabled = true;

            var depthOfField = m_Profile.depthOfField.settings;
            depthOfField.focusDistance = 2.0f;
            m_Profile.depthOfField.settings = depthOfField;
            m_Profile.depthOfField.enabled = true;

            if (audioSource.isPlaying)
            {
                audioSource.volume = 0.95f;
            }

        }//END: "if" the player has drank 2 BEERs



        if (m_player.Cups.Count <= 6)
        {
            var motionBlur = m_Profile.motionBlur.settings;
            motionBlur.shutterAngle = 360;
            motionBlur.sampleCount = 32;
            motionBlur.frameBlending = 0.75f;
            m_Profile.motionBlur.settings = motionBlur;
            m_Profile.motionBlur.enabled = true;

            var depthOfField = m_Profile.depthOfField.settings;
            depthOfField.focusDistance = 1.4f;
            depthOfField.focalLength = 56;
            m_Profile.depthOfField.settings = depthOfField;
            m_Profile.depthOfField.enabled = true;

            var grain = m_Profile.grain.settings;
            grain.intensity = 0.50f;
            grain.size = 1.50f;
            m_Profile.grain.settings = grain;
            m_Profile.grain.enabled = true;
            m_Profile.grain.enabled = true;

            m_Profile.vignette.enabled = true;

            if (audioSource.isPlaying)
                audioSource.pitch = 0.92f;

        }//END: "if" the player has drank 4 BEERs



        if (m_player.Cups.Count <= 4)
        {
            var motionBlur = m_Profile.motionBlur.settings;
            motionBlur.shutterAngle = 360;
            motionBlur.sampleCount = 32;
            motionBlur.frameBlending = 1.0f;
            m_Profile.motionBlur.settings = motionBlur;
            m_Profile.motionBlur.enabled = true;

            var depthOfField = m_Profile.depthOfField.settings;
            depthOfField.focusDistance = 1.1f;
            depthOfField.focalLength = 58;
            m_Profile.depthOfField.settings = depthOfField;
            m_Profile.depthOfField.enabled = true;

            var grain = m_Profile.grain.settings;
            grain.intensity = 0.62f;
            grain.size = 1.75f;
            m_Profile.grain.settings = grain;
            m_Profile.grain.enabled = true;

            var vignette = m_Profile.vignette.settings;
            vignette.smoothness = 0.50f;
            m_Profile.vignette.settings = vignette;
            m_Profile.vignette.enabled = true;

            if (audioSource.isPlaying)
                audioSource.pitch = 0.9f;

        }//END: "if" the player has drank 6 BEERs


        if (m_player.Cups.Count <= 2)
        {
            var motionBlur = m_Profile.motionBlur.settings;
            motionBlur.shutterAngle = 360;
            motionBlur.sampleCount = 32;
            motionBlur.frameBlending = 1.0f;
            m_Profile.motionBlur.settings = motionBlur;
            m_Profile.motionBlur.enabled = true;

            var depthOfField = m_Profile.depthOfField.settings;
            depthOfField.focusDistance = 0.8f;
            depthOfField.focalLength = 60;
            m_Profile.depthOfField.settings = depthOfField;
            m_Profile.depthOfField.enabled = true;

            var grain = m_Profile.grain.settings;
            grain.intensity = 0.75f;
            grain.size = 2.00f;
            m_Profile.grain.settings = grain;
            m_Profile.grain.enabled = true;

            var vignette = m_Profile.vignette.settings;
            vignette.smoothness = 0.80f;
            m_Profile.vignette.settings = vignette;
            m_Profile.vignette.enabled = true;

            if (audioSource.isPlaying)
                audioSource.pitch = 0.8f;

        }//END: "if" the player has drank 8 BEERs


        if (m_player.Cups.Count <= 1)
        {
            var motionBlur = m_Profile.motionBlur.settings;
            motionBlur.shutterAngle = 360;
            motionBlur.sampleCount = 32;
            motionBlur.frameBlending = 1.0f;
            m_Profile.motionBlur.settings = motionBlur;
            m_Profile.motionBlur.enabled = true;

            var depthOfField = m_Profile.depthOfField.settings;
            depthOfField.focusDistance = 0.8f;
            depthOfField.focalLength = 60;
            m_Profile.depthOfField.settings = depthOfField;
            m_Profile.depthOfField.enabled = true;

            var grain = m_Profile.grain.settings;
            grain.intensity = 0.75f;
            grain.size = 2.00f;
            m_Profile.grain.settings = grain;
            m_Profile.grain.enabled = true;

            var vignette = m_Profile.vignette.settings;
            vignette.smoothness = 0.80f;
            m_Profile.vignette.settings = vignette;
            m_Profile.vignette.enabled = true;

            if (audioSource.isPlaying)
                audioSource.pitch = 0.7f;

        }//END: "if" the player has drank 8 BEERs




    }//END: Update()

    /********************************************************************
     *  END: Section to Manipulate Post Processing Effects on Camera
     *      and Game Music
     * 
     */




    void LateUpdate()
    {
        transform.position = Ball.transform.position + offset + (Vector3)drunknessDirection;

        // Clamp the distance
        Vector3 direction = (transform.position - Ball.transform.position).normalized;
        Vector3 farthestPos = Ball.transform.position + 3 * direction;

        float xMax, xMin, yMax, yMin, zMax, zMin;
        if (farthestPos.x > Ball.transform.position.x)
        {
            xMax = farthestPos.x; xMin = Ball.transform.position.x;
        }
        else
        {
            xMin = farthestPos.x; xMax = Ball.transform.position.x;
        }

        if (farthestPos.y > Ball.transform.position.y)
        {
            yMax = farthestPos.y; yMin = Ball.transform.position.y;
        }
        else
        {
            yMin = farthestPos.y; yMax = Ball.transform.position.y;
        }

        if (farthestPos.z > Ball.transform.position.z)
        {
            zMax = farthestPos.z; zMin = Ball.transform.position.z;
        }
        else
        {
            zMin = farthestPos.z; zMax = Ball.transform.position.z;
        }

        transform.position = new Vector3(
            Mathf.Clamp(transform.position.x, xMin, xMax),
           Mathf.Clamp(transform.position.y, yMin, yMax),
           Mathf.Clamp(transform.position.z, zMin, zMax));


    }

    public void ResetDrunkness(float drunkenness = 0)
    {
        SetDrunkness(drunkenness);
        drunknessDirection = new Vector2();
        StopCoroutine(ChangeDrunknessDirection());
        StartCoroutine(ChangeDrunknessDirection());
    }

    public void SetCameraOffset()
    {
        offset = transform.position - Ball.transform.position;

    }

    public void SetDrunkness(float drunkenness, bool temporary = false)
    {
        if (!temporary)
            currentDrunkness = drunkenness;
        float d = Mathf.Clamp(drunkenness, 0, 20);



        float devisionFactor;
        if (drunkenness < 20)
        {
            devisionFactor = 90000;
        }
        else if (drunkenness < 50)
        {
            devisionFactor = 72000;
        }
        else if (drunkenness < 75)
        {
            devisionFactor = 50000;
        }
        else
        {
            devisionFactor = 20000;
        }

        drunknessIncrementX = Mathf.Pow(d, 1.1f) / devisionFactor;
        drunknessIncrementY = Mathf.Pow(d, 1.1f) / devisionFactor;

        drunkenBounds.extents = new Vector3(
            Mathf.Pow(1.3f, d / 7) / 5,
            Mathf.Pow(1.1f, d / 7) / 5, 0);

        //drunknessIncrementX = Mathf.Pow(drunkenness, 1.1f) / 5000;
        //drunknessIncrementY = Mathf.Pow(drunkenness, 1.1f) / 5000;
    }

    private void FixedUpdate()
    {
        // Adjust the drunkness direction
        drunknessDirection += new Vector2(drunknessIncrementX, drunknessIncrementY);
        drunknessDirection.x = Mathf.Clamp(drunknessDirection.x, drunkenBounds.min.x, drunkenBounds.max.x);
        drunknessDirection.y = Mathf.Clamp(drunknessDirection.y, drunkenBounds.min.y, drunkenBounds.max.y);

        //drunknessDirection += new Vector2(drunknessIncrementX, drunknessIncrementY);
        //drunknessDirection.x = Mathf.Clamp(drunknessDirection.x, -2, 2);
        //drunknessDirection.y = Mathf.Clamp(drunknessDirection.y, -2, 2);
    }

    IEnumerator ChangeDrunknessDirection()
    {
        for (; ; )
        {
            //float chanceX = Random.Range(-1f, 1f);
            //float chanceY = Random.Range(-1f, 1f);
            //if (chanceX > 0) drunknessIncrementX = Mathf.Abs(drunknessIncrementX);
            //else drunknessIncrementX = -Mathf.Abs(drunknessIncrementX);
            //if (chanceY > 0) drunknessIncrementY = Mathf.Abs(drunknessIncrementY);
            //else drunknessIncrementY = -Mathf.Abs(drunknessIncrementY);

            if (drunknessDirection.x >= drunkenBounds.max.x)
            {
                drunknessIncrementY = -Mathf.Abs(drunknessIncrementY);
            }
            if (drunknessDirection.y <= drunkenBounds.min.y)
            {
                drunknessIncrementX = -Mathf.Abs(drunknessIncrementX);
            }
            if (drunknessDirection.x <= drunkenBounds.min.x)
            {
                drunknessIncrementY = Mathf.Abs(drunknessIncrementY);
            }
            if (drunknessDirection.y >= drunkenBounds.max.y)
            {
                drunknessIncrementX = Mathf.Abs(drunknessIncrementX);
            }

            float minWS = 1;
            float d = Mathf.Clamp(currentDrunkness, 0, 70);
            float rt = currentDrunkness != 0 ? Mathf.Clamp(50 / d, minWS, 5f) : 5f;
            float ws = Random.Range(Mathf.Clamp(rt - 1, minWS, float.MaxValue), rt);
            yield return new WaitForSeconds(ws);
        }
    }

    IEnumerator FocusWhenDrunk()
    {
        float focusedDrunkenness = currentDrunkness * .5f;
        //float originalDrunkenness = currentDrunkness;
        SetDrunkness(focusedDrunkenness, true);

        yield return new WaitForSeconds(Random.Range(1f, 5f));

        SetDrunkness(currentDrunkness);

    }
}
