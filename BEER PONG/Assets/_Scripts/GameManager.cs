﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : Singleton<GameManager>
{
    // *******************************************************
    // Begin Member Fields/Variables Section
    // *******************************************************

    public enum GameState { Prepration, OnGoing }

    [SerializeField]
    [Range(0.0f, 1.0f)]
    private float trajectoryOffset = .1f;

    [SerializeField]
    public GameState gameState;

    List<Character> players;

    [SerializeField]
    public int indexOfActivePlayer;

    BallController ball;
    CueController cue;
    LandedLine landLRed;
    LandedLineB landLBlue;

    GameObject uiCanvas;

    bool mouseFreeMove;
    bool ableToProceedToNextRound;
    bool clickedToProceedToNextRound;
    bool fireButtonDown;

   List<Coroutine> currentWaitingForNextRoundCoroutines;

    // ********************************************************
    // END: Member Fields/Variables Section
    // ********************************************************





    // ********************************************************
    // BEGIN: Properties Section
    // ********************************************************

    /// <summary>
    /// Property "Active Player" - Returns a reference to the "Character" at the current Index of the member field "players" array
    /// </summary>
    public Character ActivePlayer
    {
        get
        {
            return players[indexOfActivePlayer];
        }
    }


    /// <summary>
    /// Property - "Players" - Returns the Player List
    /// </summary>
    public List<Character> Players
    {
        get
        {
            return players;
        }
    }



    /// <summary>
    /// Property "BallGameState" - returns a reference to the member field enumeration "gameState"
    /// </summary>
    public GameState BallGameState
    {
        get
        {
            return gameState;
        }
    }


    /// <summary>
    /// Property "Ball" - returns a reference to the member field GameObject "Ball"
    /// </summary>
    public BallController Ball
    {
        get
        {
            return ball;
        }
    }


    /// <summary>
    /// Property "Cue" - returns a reference to the member field Monobehavior "CueController"
    /// </summary>
    public CueController Cue
    {
        get
        {
            return cue;
        }
    }


    /// <summary>
    /// Property "MouseFreeMove" - Returns a reference to the member field boolean "mouseFreeMove"
    /// Sets the boolean to TRUE or FALSE.
    /// TRUE displays the Mouse Cursor.
    /// FALSE locks the Mouse Cursor, and does not display the cursor.
    /// </summary>
    public bool MouseFreeMove
    {
        get { return mouseFreeMove; }
        set
        {
            if (value)
            {
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            }
            else
            {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }
            mouseFreeMove = value;
        }
    }

    public bool AbleToProceedToNextRound
    {
        get
        {
            return ableToProceedToNextRound;
        }

        set
        {
            ableToProceedToNextRound = value;
        }
    }


    // ********************************************************
    // END: Propertries Section
    // ********************************************************




    /// <summary>
    /// 
    /// </summary>
    public void NextRound(float afterSeconds = 0f)
    {
        if (!CheckWinningState())
        {
            if (gameState == GameState.OnGoing)
            {
                //gameState = GameState.Prepration;
                //Invoke("InitiateNextRound", 2f);
                currentWaitingForNextRoundCoroutines.Add(StartCoroutine(InitiateNextRoundAfterSeconds(afterSeconds)));
            }
        }
    }

    // ********************************************************

    bool CheckWinningState()
    {
        for (int i = 0; i < players.Count; i++)
        {
            if (players[i].Cups.Count < 1)
            {
                // Out!
                StartCoroutine(DisplayLoseMessage(players[i]));

                // This player runs out of cups!
                players.RemoveAt(i);
            }
        }

        if (players.Count == 1)
        {
            // Winner!
            LineRenderer lineRenderer = ball.GetComponent<LineRenderer>(); //deletes the linerenderer when winning
            lineRenderer.SetVertexCount(0);
            StartCoroutine(DisplayWinMessage(players[0]));
            return true;
        }

        if (players.Count < 1)
        {
            // No winner. (Currently imposible)
            StartCoroutine(DisplayDrawMessage());
            return true;
        }

        return false;
    }

    // ********************************************************

    IEnumerator InitiateNextRoundAfterSeconds(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        InitiateNextRound();
    }

    void InitiateNextRound()
    {
        gameState = GameState.Prepration;
        StopCurrentWaitingForNextRoundCoroutine();

        // Set the active player the next one.
        indexOfActivePlayer = (indexOfActivePlayer + 1) % players.Count;
        ResetForPreparation();
        //StopCurrentWaitingForNextRoundCoroutine();


        //print("Game manager gets the ball:" + ball);

        //print("next round!");
    }

    private void StopCurrentWaitingForNextRoundCoroutine()
    {
        if (currentWaitingForNextRoundCoroutines.Count>0)
        {
            foreach(Coroutine coroutine in currentWaitingForNextRoundCoroutines)
            {
                StopCoroutine(coroutine);
            }
            currentWaitingForNextRoundCoroutines.Clear();
        }
    }

    // ********************************************************

    public void ResetTheRound()
    {
        if (gameState == GameState.Prepration) return;

        gameState = GameState.Prepration;
        ResetForPreparation();
    }

    //public void TestSwitchTo(int playerNum)
    //{
    //    indexOfActivePlayer = Mathf.Clamp(playerNum, 0, 1);
    //    gameState = GameState.Prepration;
    //    StopCurrentWaitingForNextRoundCoroutine();
    //    ResetForPreparation();
    //}

    // ********************************************************

    private void ResetForPreparation()
    {
        //landLBlue.SetBallLandedLineBlue();
        //landLRed.SetBallLandedLineRed();
        //SetBallLandedLineBlue();
        //SetBallLandedLineRed();
        SetBallForActivePlayer();
        SetCameraForActivePlayer();
        SetCueForActivePlayer();

        AbleToProceedToNextRound = false;
        clickedToProceedToNextRound = false;
        fireButtonDown = false;
    }

    // ********************************************************

    public Vector3 RecordBallLanded()
    {
        return ball.transform.position;
    }

    /*public void SetBallLandedLineRed()
    {
        if (indexOfActivePlayer == 1 && gameState != GameState.OnGoing )
        {
            LineRenderer lineRenderer = GetComponent<LineRenderer>();
            lineRenderer.SetVertexCount(2);
            lineRenderer.material.color = Color.red;
            lineRenderer.SetPosition(1, RecordBallLanded());
        }

    }

    public void SetBallLandedLineBlue()
    {
        if (indexOfActivePlayer == 0 && gameState != GameState.OnGoing)
        {
            LineRenderer lineRenderer = GetComponent<LineRenderer>();
            lineRenderer.SetVertexCount(2);
            lineRenderer.material.color = Color.blue;
            lineRenderer.SetPosition(1, RecordBallLanded());
        }

    }*/

    private void UpdateTrajectory(Vector3 initialPosition, Vector3 initialVelocity, Vector3 gravity)
    {
        int numSteps = 30; // How large the trace is
        float timeDelta = 1.0f / initialVelocity.magnitude; // Amount of seconds the after trace is made

        LineRenderer lineRenderer = ball.GetComponent<LineRenderer>();
        lineRenderer.SetVertexCount(numSteps);

        Vector3 position = initialPosition;
        Vector3 velocity = initialVelocity;
        for (int i = 0; i < numSteps; ++i)
        {
            lineRenderer.SetPosition(i, position);

            position += velocity * timeDelta + 0.5f * gravity * timeDelta * timeDelta; //-= FOR TRAILING += FOR PREDICTION
            velocity += gravity * timeDelta;
        }
    }

    // ********************************************************

    private void Start()
    {
        players = new List<Character>()
        {
            GameObject.FindGameObjectWithTag("Player1").GetComponent<Character>(),
            GameObject.FindGameObjectWithTag("Player2").GetComponent<Character>()
        };

        indexOfActivePlayer = 0;

        GameObject b = GameObject.FindGameObjectWithTag("Ball");
        //GameObject LandL = GameObject.FindGameObjectWithTag("RedLine");
        //GameObject LandLB = GameObject.FindGameObjectWithTag("BlueLine");
        //if (!b) GetBallFromActivePlayer();
        /*else*/
        ball = b.GetComponent<BallController>();
        //landLRed = GameObject.FindGameObjectWithTag("RedLine").GetComponent<LandedLine>();
        //landLBlue = GameObject.FindGameObjectWithTag("BlueLine").GetComponent<LandedLineB>();
        cue = GameObject.FindGameObjectWithTag("Cue").GetComponent<CueController>();

        uiCanvas = FindObjectOfType<Canvas>().gameObject;

        gameState = GameState.Prepration;

        foreach (Character player in players)
        {
            player.UpdateCupDisplay();
            player.UpdateDizzinessDisplay();
        }

        currentWaitingForNextRoundCoroutines = new List<Coroutine>();
    }

    // ********************************************************

    private void Update()
    {
        // If the mouse is over a UI element in the scene, do nothing. 
        // This means when the player clicks a UI button, it won't trigger this mouse click event.
        if (EventSystem.current.IsPointerOverGameObject()) return;

        if (Input.GetButton("Fire1"))
        {
            if (BallGameState == GameState.Prepration)
            {
                ball.Thrust += 0.5f;
                fireButtonDown = true;
            }
        }

        if (Input.GetButtonUp("Fire1"))
        {
            if (BallGameState == GameState.Prepration)
            {
                if (fireButtonDown)
                {
                    Vector3 movement = cue.transform.forward;
                    ball.GetComponent<Rigidbody>().isKinematic = false;
                    ball.GetComponent<Rigidbody>().AddForce(movement * ball.Thrust);
                    cue.gameObject.SetActive(false);
                    gameState = GameState.OnGoing;
                    ball.BallLaunched = true;

                    MouseFreeMove = true;

                    fireButtonDown = false;
                }
            }
        }

        if (Input.GetButtonDown("Fire3"))
        {
            if (BallGameState == GameState.Prepration)
            {
                cue.Focus = true;
                CameraController.Instance.Focus = true;
            }
        }

        if (Input.GetButtonUp("Fire3"))
        {
            if (BallGameState == GameState.Prepration)
            {
                cue.Focus = false;
                CameraController.Instance.Focus = false;
            }
        }

        // Test
        //if (Input.GetKeyUp(KeyCode.R))
        //{
        //    TestSwitchTo(0);
        //}
        //if (Input.GetKeyUp(KeyCode.B))
        //{
        //    TestSwitchTo(1);
        //}

        //if (Input.GetKeyUp(KeyCode.N))
        //{
        //    InitiateNextRound();
        //}

        if (Input.GetButtonUp("NextRound"))
        {
            if (!clickedToProceedToNextRound && ableToProceedToNextRound)
            {
                clickedToProceedToNextRound = true;
                StopCurrentWaitingForNextRoundCoroutine();
                NextRound();
            }
        }
    }

    // ********************************************************

    private void FixedUpdate()
    {
        float horizontalInput = Input.GetAxisRaw("Mouse X");
        float verticalInput = Input.GetAxisRaw("Mouse Y");

        cue.HorizontalOffset += horizontalInput * cue.MouseSensitivity * Time.deltaTime;
        if (indexOfActivePlayer == 0)
        {
            cue.VerticalOffset += verticalInput * cue.MouseSensitivity * Time.deltaTime;
        }
        else
        {
            cue.VerticalOffset -= verticalInput * cue.MouseSensitivity * Time.deltaTime;
        }

        UpdateTrajectory(ball.transform.position, cue.transform.forward * ball.Thrust * trajectoryOffset, Physics.gravity);
        if (!ball.Rb.isKinematic)
        {
            LineRenderer lineRenderer = ball.GetComponent<LineRenderer>(); //deletes the linerenderer
            lineRenderer.SetVertexCount(0);
        }

    }

    // ********************************************************

    private void LateUpdate()
    {
        if (indexOfActivePlayer == 0)
        {
            cue.UpdateCue(Vector3.forward);
        }
        else
        {
            cue.UpdateCue(Vector3.back);

        }
    }

    // ********************************************************

    private void SetBallForActivePlayer()
    {
        ball.ResetStatus();

        ball.transform.position = ActivePlayer.BallStartPosition;
    }

    // ********************************************************

    private void SetCameraForActivePlayer()
    {
        Camera.main.transform.position = ActivePlayer.CameraStartPosition;
        Camera.main.transform.eulerAngles = new Vector3(Camera.main.transform.eulerAngles.x, (indexOfActivePlayer * 180) % 360, 0);

        CameraController cameraController = Camera.main.GetComponent<CameraController>();
        cameraController.SetCameraOffset();
        cameraController.ResetDrunkness(ActivePlayer.CurrentDrunkenness);
    }

    // ********************************************************


    private void SetCueForActivePlayer()
    {
        cue.gameObject.SetActive(true);
        cue.RestCue(ActivePlayer.CurrentDrunkenness);
    }


    // ********************************************************

    IEnumerator DisplayLoseMessage(Character character)
    {
        Text loseMessage = uiCanvas.transform.GetChild(1).gameObject.GetComponent<Text>();
        loseMessage.text = $"{character.CharacterName}{Environment.NewLine} is out!";
        loseMessage.gameObject.SetActive(true);

        yield return new WaitForSeconds(2);

        loseMessage.gameObject.SetActive(false);
    }

    // ********************************************************


    IEnumerator DisplayWinMessage(Character character)
    {
        //Print that the collision was detected to the console for debugging purposes
        Text winMessage = uiCanvas.transform.GetChild(0).gameObject.GetComponent<Text>();
        winMessage.text = $"{players[0].CharacterName}{Environment.NewLine} wins!";
        winMessage.gameObject.SetActive(true);

        yield return new WaitForSeconds(3);

        GoBackHome();
    }


    // ********************************************************

    IEnumerator DisplayDrawMessage()
    {
        Text drawMessage = uiCanvas.transform.GetChild(0).gameObject.GetComponent<Text>();
        drawMessage.text = "Draw!";
        drawMessage.gameObject.SetActive(true);

        yield return new WaitForSeconds(3);

        GoBackHome();
    }


    // ********************************************************

    public void GoBackHome()
    {
        SceneManager.LoadScene("Scene00");
    }

    // ********************************************************




}//END: GameManager Class


// ********************************************************

///// <summary>
///// Deprecated.
///// If there is no ball in the scene, generate one from the active player;
///// </summary>
//private void GetBallFromActivePlayer()
//{
//    ball = Instantiate(ActivePlayer.BallPrefab, ActivePlayer.BallStartPosition, Quaternion.identity).GetComponent<BallController>();
//    ball.tag = "Ball";
//    ball.GetComponent<Rigidbody>().isKinematic = true;
//}