﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGMusic : MonoBehaviour {

    AudioSource audioSource;

    private void Awake()
    {
        //InactivateBall();
        audioSource = GetComponent<AudioSource>();
    }


    private void Start()
    {
        audioSource.Play();
    }

}
