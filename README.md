# README #

This README is about our proud group project Beer Pong! 

### What is this repository for? ###

Beer Pong is a game where two players try to throw Ping Pong balls into their rival's cups. The player must drink up the beer in the cup which a ball is plunked into. As the player drinking more beers the player will get more drunken, which makes the game funnier and harder to aim.
Your goal is to make your opponent drink up all of his cups and believe me, he will NOT feeling himself after drinking up that amount of alcohol!

Features of this game: Drunkenness simulation, ball throwing system, bar style surrounding and UI.

### How do I get set up? ###

Download the project.
Run in Unity.
That's it. ;)

### Contribution guidelines ###

Credits:
Original Idea: Tony
Design: Tony, Clarence, Josiah, Wenqi
Programming:  Clarence, Tony, Josiah
Sounds: Josiah, Tony
Model & UI style: Wenqi
